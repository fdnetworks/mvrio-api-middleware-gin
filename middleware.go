package mvrio_api_middleware_gin

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/fdnetworks/mvrio-api-middleware-gin/commons/errors"
	"gitlab.com/fdnetworks/mvrio-api-middleware-gin/commons/responses"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)


type Header struct {
	XFDNRequestID     string `valid:"uuidv4,required"`
	XFDNApiVersion    string `valid:"semver,required"`
	XFDNClientVersion string `valid:"semver,required"`
	XFDNPlatformName  string `valid:"required"`
	XFDNClientName    string `valid:"required"`

	// Optional
	XFDNSignature string `valid:"optional"`
	XFDNTime      string `valid:"int,optional"`
	Authorization  string `valid:"optional"`
}

func AuthMiddleware() gin.HandlerFunc {
	response := ResponseJwt{}
	return func(c *gin.Context) {
		t := c.Request.Header.Get("Authorization")
		if len(t) == 0 {
			c.JSON(http.StatusUnauthorized, responses.ResponseUnauthorized("Token required"))
			c.Abort()
			return
		}
		if err := httpRequest(os.Getenv("USER_SERVICE_URL")+"/validate-token?token="+t, response); err != nil {
			c.JSON(http.StatusUnauthorized, responses.ResponseUnauthorized("Invalid authorization token"))
			c.Abort()
			return
		}
		if response.Payload.Data.ID < 1 || response.Payload.Data.Username == "" {
			c.JSON(http.StatusUnauthorized, responses.ResponseUnauthorized("Token has empty payload, cannot identify user"))
			c.Abort()
			return
		}
		c.Next()
	}
}

// func FDBasic() gin.HandlerFunc {
// 	return func(c *gin.Context) {
// 		header := Header{
// 			XFDNRequestID:     c.Request.Header.Get("X-FDN-Request-ID"), // UUID of Request id
// 			XFDNApiVersion:    c.Request.Header.Get("X-FDN-Api-Version"), // API Version Request
// 			XFDNClientVersion: c.Request.Header.Get("X-FDN-Client-Version"), // Client Version
// 			XFDNPlatformName:  c.Request.Header.Get("X-FDN-Platform-Name"), // Platform name e.g: android, ios, or web.
// 			XFDNClientName:    c.Request.Header.Get("X-FDN-Client-Name"), // Complete meta Client name e.g: Android 10.1
// 			XFDNSignature:    c.Request.Header.Get("X-FDN-Signature"), // Request signature
// 			XFDNTime:         c.Request.Header.Get("X-FDN-Time"), // Current request time in timestamp
// 			Authorization:     c.Request.Header.Get("Authorization"), // Authorization access token
// 		}
//
// 		c.Next()
// 	}
// }

// httpRequest to make a httpRequest and return object
func httpRequest(url string, obj interface{}) (err error) {
	var myClient = &http.Client{Timeout: 10 * time.Second}
	r, err := myClient.Get(url)
	if err != nil {
		err = errors.ErrInternalServerError
		return err
	}
	defer r.Body.Close()

	body, readErr := ioutil.ReadAll(r.Body)
	if readErr != nil {
		err = errors.ErrInternalServerError
		return err
	}
	err = json.Unmarshal(body, &obj)
	if err != nil {
		log.Println(err)
		return
	}

	return
}
