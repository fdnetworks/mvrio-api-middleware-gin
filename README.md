# Base API Lib
This packages is collection of base response, errors, struct, etc. That used on Female Daily Network services based on Go Lang.

## In This package
- Base responses
- Errors constant
- Basic Security middleware
- Base Struct type
