package responses

import (
	"github.com/gin-gonic/gin"
	"math"
	"net/http"
)

type meta struct {
	Code         int    `json:"code"`
	Message      string `json:"message"`
	ErrorMessage string `json:"error"`
}

type payload struct {
	Data interface{} `json:"data"`
	//Pagination Pagination  `json:"pagination"`
}

type emptyPayload struct{}

type Pagination struct {
	Page      int    `json:"page,omitempty"`
	Count     int    `json:"count,omitempty"`
	Total     int    `json:"total,omitempty"`
	Limit     int    `json:"limit,omitempty"`
	TotalPage int    `json:"total_page,omitempty"`
	NextID    string `json:"next_id,omitempty"`
}

// ResponseSuccess used for handling response with http code 200 (singular data payload)
func ResponseSuccess(data interface{}) gin.H {
	code := http.StatusOK
	return gin.H{
		"meta":    meta{Code: code, Message: http.StatusText(code), ErrorMessage: ""},
		"payload": payload{Data: data},
	}
}

// ResponsesDataNotFound used for handling responses with http 404 but for data not found
func ResponseDataNotFound(message string) gin.H {
	code := http.StatusNotFound
	return gin.H{
		"meta":    meta{Code: code, Message: http.StatusText(code), ErrorMessage: message},
		"payload": payload{Data: make(map[string]interface{})},
	}
}

// ResponseSuccess used for handling response with http code 200 (singular data payload)
func ResponseEmptyData(errMsg string, pg Pagination) gin.H {
	code := http.StatusBadRequest
	return gin.H{
		"meta":    meta{Code: code, Message: http.StatusText(code), ErrorMessage: errMsg},
		"payload": payload{Data: make(map[string]interface{})},
	}
}

// ResponseCreated user for handling response with http code 201
func ResponseCreated(data interface{}) gin.H {
	code := http.StatusCreated
	return gin.H{
		"meta":    meta{Code: code, Message: http.StatusText(code), ErrorMessage: ""},
		"payload": payload{Data: data},
	}
}

// ResponseBadRequest used for handling response with http code 400
func ResponseBadRequest(errMsg string) gin.H {
	code := http.StatusBadRequest
	return gin.H{
		"meta":    meta{Code: code, Message: http.StatusText(code), ErrorMessage: errMsg},
		"payload": emptyPayload{},
	}
}

// ResponseUnauthorized used for handling response with http code 401
func ResponseUnauthorized(errMsg string) gin.H {
	code := http.StatusUnauthorized
	return gin.H{
		"meta":    meta{Code: code, Message: http.StatusText(code), ErrorMessage: errMsg},
		"payload": emptyPayload{},
	}
}

// ResponseNotFound used for handling response with http code 404
func ResponseNotFound() gin.H {
	code := http.StatusNotFound
	return gin.H{
		"meta":    meta{Code: code, Message: http.StatusText(code), ErrorMessage: ""},
		"payload": emptyPayload{},
	}
}

// ResponseInternalServerError used for handling response with http code 500
func ResponseInternalServerError() gin.H {
	code := http.StatusInternalServerError
	return gin.H{
		"meta":    meta{Code: code, Message: http.StatusText(code), ErrorMessage: "An error(s) has occurred"},
		"payload": emptyPayload{},
	}
}

func GeneratePagination(page int, count int, total int, limit int, nextID string) Pagination {
	return Pagination{
		Count:     count,
		Total:     total,
		Limit:     limit,
		TotalPage: int(math.Ceil(float64(total) / float64(limit))),
		Page:      page,
		NextID:    nextID,
	}
}
