package errors

import "errors"

var (
	InternalServerError = "Internal server error"
	DataNotFound        = "Your requested data is not found"
	RouteNotFound       = "Route not found"
	Conflict            = "Data already exist"
	BadParamInput       = "Given param is not valid"
	ProductsNotFound    = "Some product not found" // @TODO should be deprecated and move to the service

	ErrInternalServerError = errors.New(InternalServerError)
	ErrDataNotFound        = errors.New(DataNotFound)
	ErrConflict            = errors.New(Conflict)
	ErrBadParamInput       = errors.New(BadParamInput)
	ErrProductsNotFound    = errors.New(ProductsNotFound)
	ErrRouteNotFound       = errors.New(RouteNotFound)
)
