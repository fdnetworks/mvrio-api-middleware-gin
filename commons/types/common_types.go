package types

import "time"

// BaseModel struct
type BaseModel struct {
	ID        uint       `json:"uint"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at"`
}

type LoadMorePagination struct {
	Total  int `json:"total"`
	Count  int `json:"count"`
	NextID int `json:"next_id"`
	Limit  int `json:"limit"`
}

// BasicPagination struct
type BasicPagination struct {
	Total     int `json:"total"`
	Count     int `json:"count"`
	Page      int `json:"page"`
	TotalPage int `json:"total_page"`
	Limit     int `json:"limit"`
}

// Meta struct
type Meta struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// BaseResponse struct
type BaseResponse struct {
	Data       interface{} `json:"data"`
	Meta       Meta        `json:"meta"`
	Error      string      `json:"error"`
	Pagination interface{} `json:"pagination,omitempty"`
}
