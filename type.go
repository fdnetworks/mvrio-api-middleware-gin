package mvrio_api_middleware_gin

type ResponseJwt struct {
	Meta    Meta    `json:"meta"`
	Payload Payload `json:"payload"`
}

type Meta struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Error   string `json:"error"`
}

type Payload struct {
	Data Data `json:"data"`
}

type Data struct {
	ID        int    `json:"id"`
	Username  string `json:"username"`
	EntryDate string `json:"entry_date"`
}
